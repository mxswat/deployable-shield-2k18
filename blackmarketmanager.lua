function BlackMarketManager:equipped_armor(chk_armor_kit, chk_player_state)
	if chk_player_state and managers.player:current_state() == "civilian" then
		return self._defaults.armor
	end

	local armor = nil

	for armor_id, data in pairs(tweak_data.blackmarket.armors) do
		armor = Global.blackmarket_manager.armors[armor_id]

		if armor.equipped and armor.unlocked and armor.owned then
			local forced_armor = self:forced_armor()

			if forced_armor then
				return forced_armor
			end

			return armor_id
		end
	end

	return self._defaults.armor
end
