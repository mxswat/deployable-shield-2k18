local DepShield_equipmentstweakdata_init_original = EquipmentsTweakData.init
function EquipmentsTweakData:init()
	DepShield_equipmentstweakdata_init_original(self)
	self.armor_kit = {
		icon = "equipment_armor_kit",
		quantity = { 1 },
		text_id = "debug_equipment_armor_kit",
		deploying_text_id = "hud_equipment_equipping_armor_kit",
		description_id = "des_armor_kit",
		use_function_name = "use_armor_kit",
		action_timer = 2,
		deploy_time = 2,
		visual_object = "g_armorbag",
		limit_movement = true,
		sound_start = "bar_armor",
		sound_interupt = "bar_armor_cancel",
		sound_done = "bar_armor_finished",
		dummy_unit = "units/payday2/characters/ene_acc_shield_lights/ene_acc_shield_lights",
	}
end
