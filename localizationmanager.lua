local DepShield_localizationmanager_text_original = LocalizationManager.text
function LocalizationManager:text(string_id, ...)
	local res = {
		bm_equipment_armor_kit = "Deployable Shield",
		menu_equipment_armor_kit = "Deployable Shield",
		debug_equipment_armor_kit = "Deployable Shield",
		bm_equipment_armor_kit_desc = "You bring some extra protection.\n\nThis shield will stop any projectile coming at you, or from you, while still allowing people to pass through it.\n\nIt uses a really advanced technology that will allow you to redeploy it elsewhere, without having to pick it up beforehand.",
		hud_equipment_equipping_armor_kit = "Deploying Shield",
		menu_deckall_6_desc = "Unlocks a deployable shield equipment for you to use. The deployable shield can be used as a cover during a heist.\n\nIncreases your ammo pickup to ##135%## of the normal rate.",
	}

	return res[string_id] or DepShield_localizationmanager_text_original(self, string_id, ...)
end
