ShieldBase = ShieldBase or class(UnitBase)
ShieldBase.shields = {}
blt.forcepcalls(true)
function PlayerEquipment:valid_shield_placement(equipment_id, equipment_data)
	local from = self._unit:movement():m_head_pos()
	local to = from + self._unit:movement():m_head_rot():y() * 220
	local ray = self._unit:raycast("ray", from, to, "slot_mask", managers.slot:get_mask("trip_mine_placeables"), "ignore_unit", {})
	local valid = ray and true or false
	if ray then
		local pos = ray.position
		local shield_pos = Vector3(pos.x, pos.y + 20, pos.z + 115)
		local rot = self._unit:movement():m_head_rot()

		rot = Rotation(rot:yaw(), 0, 0)
		if not alive(self._dummy_unit) then
			self._dummy_unit = World:spawn_unit(Idstring(equipment_data.dummy_unit), shield_pos, rot)
			self:_disable_contour(self._dummy_unit)
			for i = 0, self._dummy_unit:num_bodies() - 1 do
				self._dummy_unit:body(i):set_keyframed()
				self._dummy_unit:body(i):set_enabled(false)
			end
		end
		self._dummy_unit:set_position(shield_pos)
		self._dummy_unit:set_rotation(rot)
		valid = valid and math.dot(ray.normal, math.UP) > 0.25
		local find_start_pos, find_end_pos, find_radius
		if equipment_id == "armor_kit" then
			find_start_pos = shield_pos + math.UP * 20
			find_end_pos = shield_pos + math.UP * 21
			find_radius = 12
		end
		local bodies = self._dummy_unit:find_bodies("intersect", "capsule", find_start_pos, find_end_pos, find_radius, managers.slot:get_mask("trip_mine_placeables") + 14 + 25)
		for _, body in ipairs(bodies) do
			if body:unit() ~= self._dummy_unit and body:has_ray_type(Idstring("body")) then
				valid = false
			else
			end
		end
	end
	if alive(self._dummy_unit) then
		self._dummy_unit:set_enabled(valid)
	end
	return valid and ray
end

function ShieldBase:spawn(peer_id, pos, rot, must_not_block_player)
	local unit = self.shields[peer_id]
	if unit and alive(unit) then
		unit:set_position(pos)
		unit:set_rotation(rot)
	else
		local tier_4 = "units/pd2_dlc_vip/characters/ene_acc_shield_vip/ene_acc_shield_vip"
		local tier_2 = "units/payday2/characters/ene_acc_shield_lights/ene_acc_shield_lights"
		local tier_1 = "units/payday2/characters/ene_acc_shield_small/shield_small"
		unit = World:spawn_unit(Idstring(tier_1), pos, rot)
		self.shields[peer_id] = unit
	end

	if must_not_block_player then
		unit:damage():run_sequence_simple("enable_body")
	end

	for i = 0, unit:num_bodies() - 1 do
		unit:body(i):set_keyframed()
	end

	return unit
end

function PlayerEquipment:use_armor_kit()
	local ray = self:valid_shield_placement("armor_kit")
	if ray then
		local peer_id = managers.network:session():local_peer():id()
		local ray_pos = ray.position
		local shield_pos = Vector3(ray_pos.x, ray_pos.y + 20, ray_pos.z + 115)
		local yaw = self._unit:movement():m_head_rot():yaw()
		local shield_rot = Rotation(yaw, 0, 0)

		local my_shield_unit = ShieldBase:spawn(peer_id, shield_pos, shield_rot, false)
		if my_shield_unit then
			LuaNetworking:SendToPeers("DepShieldHERE", json.encode({shield_pos.x, shield_pos.y, shield_pos.z, yaw, 0, 0}))
			PlayerStandard.say_line(self, "f31x_any") -- shield shout
		end
	end

	return false
end

Hooks:Add("NetworkReceivedData", "NetworkReceivedData_DepShield", function(sender, messageType, data)
	if messageType == "DepShieldHERE" then
		if data and data ~= "" then
			local dec = json.decode(data)
			ShieldBase:spawn(sender, Vector3(dec[1], dec[2], dec[3]), Rotation(dec[4], dec[5], dec[6]), true)
		end
	end

	if messageType == "DepShield?" then
		local msg = ""
		local my_shield = ShieldBase.shields[managers.network:session():local_peer():id()]
		if my_shield and alive(my_shield) then
			local pos = my_shield:position()
			msg = json.encode({pos.x, pos.y, pos.z, my_shield:rotation():yaw(), 0, 0})
		end
		LuaNetworking:SendToPeer(sender, "DepShieldHERE", msg)
	end
end)

Hooks:Add("BaseNetworkSessionOnLoadComplete", "BaseNetworkSessionOnLoadComplete_DepShield", function(local_peer, id)
	ShieldBase.shields = {}
	LuaNetworking:SendToPeers("DepShield?", "")
end)

Hooks:Add("BaseNetworkSessionOnPeerRemoved", "BaseNetworkSessionOnPeerRemoved_DepShield", function(peer, peer_id, reason)
	local shield = ShieldBase.shields[peer_id]
	if shield and alive(shield) then
		World:delete_unit(shield)
		ShieldBase.shields[peer_id] = nil
	end
end)
