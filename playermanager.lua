local DepShield_playermanager_checkselectedequipmentplacementvalid_original = PlayerManager.check_selected_equipment_placement_valid
function PlayerManager:check_selected_equipment_placement_valid(player)
	local equipment_data = managers.player:selected_equipment()
	if not equipment_data then
		return false
	end
	
	if equipment_data.equipment == "armor_kit" then
		if managers.network:session():peer_by_unit(player):skills():split("-")[1] == "0_0_0_0_0" then
			return false
		end
		return player:equipment():valid_shield_placement(equipment_data.equipment, tweak_data.equipments[equipment_data.equipment]) and true or false
	end

	return DepShield_playermanager_checkselectedequipmentplacementvalid_original(self, player)
end
